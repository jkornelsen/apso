# APSO<br><br>Alternative python script organizer for Apache OpenOffice and LibreOffice

Based on an [original script from Hanya](https://forum.openoffice.org/en/forum/viewtopic.php?p=242221#p242221), APSO is an extension that will install a macro organizer dedicated to python scripts.

### Features
- localisation (by now EN, DE, FR, HU thanks to Zizi64, IT and NL thanks to Dick Groskamp)
- embedding in and extracting scripts from documents
- binding the extension with one's favorite code editor (thanks to [another script from Hanya](http://hermione.s41.xrea.com/pukiwiki/index.php?OOobbs2%2F194&word=editorkicker))
- quick editing of embedded scripts (but only in the context of the APSO extension, be careful here)
- automatic update once installed

### How to use it
Download and install the latest release like any other extension: double-click the oxt file or import it from the extension manager (menu *Tools -> Extension Manager...*), then restart the program.

The extension adds a new element *Organize python scripts* under menu *Tools/Macros* with default shortcut *Alt+Shift+F11*:

![apso_menu](img/apso_menu.png)

This new item or shortcut opens a dialog box that shows all python scripts and provides various actions:

![apso_dialog](img/apso_dialog.png)

The **Execute**  button launches the selected macro.
Actions available under **Menu** button change according the selection:
- Create module or library if selection is a container (*My Macros*, *Office Macros* or *document*)
- Create pythonpath example to import a custom module
- Edit, rename, delete a module or library
- Embed module from application (*My Macros*, *Office Macros*) into current document
- Substitute an embedded module with an external file
- Export an embedded module

To specify the code editor, choose *Options* in the Extension manager.

### Helpers
APSO comes with a small library `apso_utils` that contains some helpers for debugging purpose during macro writing.
These helpers are four functions named *msgbox*, *xray*, *mri* and *console* and can be imported by a classic import statement like 
```python
from apso_utils import msgbox
```

function | description
:---: | ---
msgbox | Like the oobasic build-in function *msgbox*, but simplified as only intended for quick debugging.<br>Signature: `msgbox(message, title='Message', boxtype='message', buttons=1, frame=None)`
xray | Launches the introspection tool [Xray](http://www.openoffice.org/fr/Documentation/Basic/) by Bernard Marcelly (must be installed first)
mri | Launches the introspection tool [MRI](https://extensions.openoffice.org/en/project/MRI) by Hanya (must be installed first)
console | Emulates, as close as possible, the python interactive shell. What one can do with such a tool:<br />- interact directly with a document and test "live" the methods of the uno api (maybe also useful for other languages, such basic);<br />- quickly verify python expressions in the context of the embedded interpreter;<br />- display the output of a script running in parallel or started from the shell (all modules visible from the office program can be imported);<br />- debug a script using pdb's runcall function (will not work with scripts embedded in documents);<br />- get quick info on any function using the builtin help() command;<br />- explore living uno objects with introspection tools such as MRI or Xray (if installed);<br />- ...

The module `apso_utils` can't be imported before APSO was first loaded. Only the *console* macro is accessible outside APSO.

<img src="img/apso_console.jpg" width="400">
