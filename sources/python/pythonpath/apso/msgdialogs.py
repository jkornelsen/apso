# coding: utf-8

import sys

def RR():
    return _RR_Container.resolver

class _RR_Container:
    """Holds one resource resolver for all modules."""
    resolver = None

def loadResourceResolver(ctx):
    if not _RR_Container.resolver:
        _RR_Container.resolver = ResourceResolver(ctx)


# Addon ID
EXTID = 'apso.python.script.organizer'

class ResourceResolver(object):
    '''Resource Resolver for localized strings'''
    def __init__(self, ctx):
        self.ctx = ctx
        self.smgr = self.ctx.getServiceManager()
        self.locale = self._get_env_locale()
        self.srwl = self._get_resource_resolver()
        self.version = self._get_ext_ver()

    def _get_ext_path(self):
        '''Get addon installation path'''
        pip = self.ctx.getByName(
            "/singletons/com.sun.star.deployment.PackageInformationProvider")
        extpath = pip.getPackageLocation(EXTID)
        # To run outside of an extension for testing, change extpath to 
        # the file path of the folder named "resource".
        #extpath = "file:///path/to/apso/sources/"
        if extpath[-1] != "/": extpath += "/"
        return extpath

    def _get_ext_ver(self):
        '''Get addon version number'''
        pip = self.ctx.getByName(
               "/singletons/com.sun.star.deployment.PackageInformationProvider")
        extensions = pip.getExtensionList()
        for ext in extensions:
            if EXTID in ext:
                return ext[1]
        return ''

    def _get_env_locale(self):
        '''Get interface locale'''
        from com.sun.star.lang import Locale
        ps = self.smgr.createInstanceWithContext(
            "com.sun.star.util.PathSubstitution", self.ctx)
        locale = Locale(ps.getSubstituteVariableValue("vlang"), "ZZ", "")
        return locale

    def _get_resource_resolver(self):
        url = self._get_ext_path() + "resource"
        handler = self.smgr.createInstanceWithContext(
            "com.sun.star.task.InteractionHandler", self.ctx)
        srwl = self.smgr.createInstanceWithArgumentsAndContext(
            "com.sun.star.resource.StringResourceWithLocation",
            (url, False, self.locale, "apsostrings", "", handler), self.ctx)
        return srwl

    def resolvestring(self, the_id):
        return self.srwl.resolveString(the_id)


if sys.version_info < (3,):
    class ErrorMessage_(Exception):
        def __init__(self, message):
            super(ErrorMessage_, self).__init__(message.encode(ENCODING))
            self.message = message
        def __unicode__(self):
            return self.message

    class ErrorAsMessage(ErrorMessage_):
        """ Tracked error message will be shown for user. """
        pass
else:
    class ErrorAsMessage(Exception):
        """ Tracked error message will be shown for user. """
        pass
class AlreadyExistsException(ErrorAsMessage):
    """Thrown if a file or folder already exists."""


class DialogBase(object):
    """ Base class for dialog. """
    def __init__(self, ctx):
        self.ctx = ctx
        self.smgr = ctx.getServiceManager()

    def create(self, name, arguments=None):
        """ Create service instance. """
        if arguments:
            return self.smgr.createInstanceWithArgumentsAndContext(
                name, arguments, self.ctx)
        else:
            return self.smgr.createInstanceWithContext(
                name, self.ctx)


class RuntimeDialogBase(DialogBase):
    """ Runtime dialog base. """

    def __init__(self, ctx):
        DialogBase.__init__(self, ctx)
        self.dialog = None

    def _result(self):
        """ Returns result. """
        return None

    def _init(self):
        """ Initialize, create dialog and controls. """

    def execute(self):
        """ Execute to show this dialog.
        None return value should mean canceled.
        """
        self._init()
        result = None
        self.dialog.setVisible(True)
        if self.dialog.execute():
            result = self._result()
        self.dialog.dispose()
        return result

    def create_control(self, name, s_type, pos, size,
                        prop_names, prop_values, full_name=False):
        """ Create and insert control. """
        if not full_name:
            s_type = "com.sun.star.awt.UnoControl" + s_type + "Model"
        dialog_model = self.dialog.getModel()
        model = dialog_model.createInstance(s_type)
        if prop_names and prop_values:
            model.setPropertyValues(prop_names, prop_values)
        dialog_model.insertByName(name, model)
        ctrl = self.dialog.getControl(name)
        ctrl.setPosSize(pos[0], pos[1], size[0], size[1], 15)
        return ctrl

    def create_dialog(self, title, pos=None, size=None, parent=None):
        """ Create base dialog. """
        dialog = self.create("com.sun.star.awt.UnoControlDialog")
        dialog_model = self.create("com.sun.star.awt.UnoControlDialogModel")
        dialog_model.ResourceResolver = RR().srwl
        dialog.setModel(dialog_model)
        if isinstance(size, tuple) and len(size) == 2:
            dialog.setPosSize(0, 0, size[0], size[1], 12)
        if isinstance(pos, tuple) and len(pos) == 2:
            dialog.setPosSize(pos[0], pos[1], 0, 0, 3)
        elif parent:
            pass
        dialog.setTitle(title)
        self.dialog = dialog

    def create_label(self, name, command, pos, size,
                        prop_names=None, prop_values=None, action=None):
        """ Create and add new label. """
        label = self.create_control(name, "Label", pos, size,
                    prop_names, prop_values)

    def create_button(self, name, command, pos, size,
                        prop_names=None, prop_values=None, action=None):
        """ Create and add new button. """
        btn = self.create_control(name, "Button", pos, size,
                    prop_names, prop_values)
        btn.setActionCommand(command)
        if action:
            btn.addActionListener(action)

    def create_edit(self, name, pos, size,
                        prop_names=None, prop_values=None):
        """ Create and add new edit control. """
        edit = self.create_control(name, "Edit", pos, size,
            prop_names, prop_values)

    def create_tree(self, name, pos, size,
                        prop_names=None, prop_values=None):
        """ Create and add new tree. """
        self.create_control(name,
            "com.sun.star.awt.tree.TreeControlModel",
            pos, size, prop_names, prop_values, full_name=True)

    def get(self, name):
        """ Returns specified control by name. """
        return self.dialog.getControl(name)

    def get_text(self, name):
        """ Returns value of Text attribute specified by name. """
        return self.dialog.getControl(name).getModel().Text

    def set_focus(self, name):
        """ Set focus to the control specified by the name. """
        self.dialog.getControl(name).setFocus()


class ErrorMessageDialog(RuntimeDialogBase):
    """ Shows error message in custom dialog with selectable text. """
    MARGIN = 3
    BUTTON_WIDTH  = 80
    BUTTON_HEIGHT = 26
    EDIT_HEIGHT = 300
    HEIGHT = EDIT_HEIGHT + MARGIN * 5 + BUTTON_HEIGHT + MARGIN
    WIDTH = 420
    EDIT_NAME = "edit_name"

    ERROR_ICON = "private:standardimage/error"

    def __init__(self, ctx, **kwds):
        RuntimeDialogBase.__init__(self, ctx)
        self.args = kwds

    def _init(self):
        args = self.args
        title = args.get("title", "")
        message = args.get("message", "")

        margin = self.MARGIN
        self.create_dialog(title, size=(self.WIDTH, self.HEIGHT))

        self.create_edit("edit_message",
            pos=(margin * 4, margin * 4),
            size=(self.WIDTH - margin * 8, self.EDIT_HEIGHT),
            prop_names=("Border", "MultiLine", "PaintTransparent",
                        "ReadOnly", "VScroll"),
            prop_values=(0, True, True, True, True))
        self.get("edit_message").getModel().Text = message
        self.create_button("btn_ok", "ok",
            pos=((self.WIDTH - self.BUTTON_WIDTH)/2,
                    self.HEIGHT - self.BUTTON_HEIGHT - margin),
            size=(self.BUTTON_WIDTH, self.BUTTON_HEIGHT),
            prop_names=("DefaultButton", "Label", "PushButtonType",),
            prop_values=(True, "OK", 1))


class SyntaxErrorMessageDialog(RuntimeDialogBase):
    """ Shows syntax error message in custom dialogwith selectable text."""
    MARGIN = 3
    BUTTON_WIDTH  = 100
    BUTTON_HEIGHT = 26
    EDIT_HEIGHT = 300
    HEIGHT = EDIT_HEIGHT + MARGIN * 5 + BUTTON_HEIGHT + MARGIN
    WIDTH = 420
    EDIT_NAME = "edit_name"

    ERROR_ICON = "private:standardimage/error"

    def __init__(self, ctx, **kwds):
        RuntimeDialogBase.__init__(self, ctx)
        self.args = kwds

    def _init(self):
        args = self.args
        title = args.get("title", "")
        message = args.get("message", "")
        ekconfig = args.get("ekconfig", "")

        margin = self.MARGIN
        self.create_dialog(title, size=(self.WIDTH, self.HEIGHT))

        self.create_edit("edit_message",
            pos=(margin * 4, margin * 4),
            size=(self.WIDTH - margin * 8, self.EDIT_HEIGHT),
            prop_names=("Border", "MultiLine", "PaintTransparent",
                        "ReadOnly", "VScroll"),
            prop_values=(0, True, True, True, True))
        self.get("edit_message").getModel().Text = message
        if ekconfig and ("{ROW}" in ekconfig[1]):
            self.create_button("btn_ok", "ok",
                pos=((self.WIDTH - self.BUTTON_WIDTH*2.5 - margin*3)/2,
                        self.HEIGHT - self.BUTTON_HEIGHT - margin),
                size=(self.BUTTON_WIDTH, self.BUTTON_HEIGHT),
                prop_names=("DefaultButton", "Label", "PushButtonType",),
                prop_values=(False, RR().resolvestring("msg28").upper(), 2))
            self.create_button("btn_open_edit", "edit",
                pos=((self.WIDTH - self.BUTTON_WIDTH*0.5 + 3*margin)/2,
                        self.HEIGHT - self.BUTTON_HEIGHT - margin),
                size=(self.BUTTON_WIDTH*1.5, self.BUTTON_HEIGHT),
                prop_names=("DefaultButton", "Label", "PushButtonType",),
                prop_values=(True,
                             '~{}'.format(RR().resolvestring('msg19').upper()),
                             1))
        else:
            self.create_button("btn_ok", "ok",
                pos=((self.WIDTH - self.BUTTON_WIDTH)/2,
                        self.HEIGHT - self.BUTTON_HEIGHT - margin),
                size=(self.BUTTON_WIDTH, self.BUTTON_HEIGHT),
                prop_names=("DefaultButton", "Label", "PushButtonType",),
                prop_values=(True, RR().resolvestring("msg28").upper(), 2))

    def execute(self):
        self._init()
        self.dialog.setVisible(True)
        result = self.dialog.execute()
        self.dialog.dispose()
        return result
