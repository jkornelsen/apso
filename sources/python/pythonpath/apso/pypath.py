# -*- coding: utf-8 -*-

"""
Support for pythonpath directory.
Normally, PythonScriptProvider does not display pythonpath,
so this module overrides that behavior to display and allow editing.

Also, set_up_pythonpath() ensures that the pythonpath directory exists,
and adds an example that uses it.
"""

from apso.msgdialogs import ErrorAsMessage, AlreadyExistsException, RR
from apso.utils import msgbox

try:
    import pythonscript
except:
    import pythonloader
    pythonscript = None
    for url, module in pythonloader.g_loadedComponents.iteritems():
        if url.endswith("script-provider-for-python/pythonscript.py"):
            pythonscript = module
    if pythonscript is None:
        raise Exception("Failed to load module pythonscript.")


class DirBrowseNode_IncludingPythonpath(pythonscript.DirBrowseNode):
    """Override DirBrowseNode so that it does not ignore the pythonpath
    directory.
    """
    def getChildNodes( self ):
        log = pythonscript.log
        try:
            log.debug("DirBrowseNode.getChildNodes() called for " + self.rootUrl)
            if not self.provCtx.sfa.exists(self.rootUrl):
                return ()
            contents = self.provCtx.sfa.getFolderContents( self.rootUrl, True )
            browseNodeList = []
            for i in contents:
                if i.endswith( ".py" ):
                    log.debug( "adding filenode " + i )
                    browseNodeList.append(
                        pythonscript.FileBrowseNode(
                            self.provCtx, i, i[i.rfind("/")+1:len(i)-3] ) )
                elif self.provCtx.sfa.isFolder( i ):
                    log.debug( "adding DirBrowseNode " + i )
                    browseNodeList.append( DirBrowseNode_IncludingPythonpath(
                        self.provCtx, i[i.rfind("/")+1:len(i)],i))
            return tuple( browseNodeList )
        except Exception as e:
            text = pythonscript.lastException2String()
            log.error(
                "DirBrowseNode error: " + str(e) + " while evaluating "
                + self.rootUrl)
            log.error(text)
            return ()

# Use our class instead of pythonscript.DirBrowseNode.
pythonscript.DirBrowseNode = DirBrowseNode_IncludingPythonpath


PPATH_EXAMPLE_CONTENT = """# -*- coding: utf-8 -*-
\"\"\"
A simple macro that shows how to import a module.
Execute either from a blank Writer document or a blank Calc spreadsheet.
\"\"\"
import uno
from loexample.hello_module import say_hello

def hello(ctx=uno.getComponentContext()):
    oDoc = XSCRIPTCONTEXT.getDocument()  # or we could use the ctx variable
    say_hello(oDoc)

g_exportedScripts = hello,
"""

HELLO_CONTENT = """# -*- coding: utf-8 -*-

import uno

def say_hello(oDoc):
    #oDoc = get_doc_from_context()
    if oDoc.supportsService("com.sun.star.text.TextDocument"):
        oText = oDoc.getText()
        oCursor = oText.createTextCursor()
        oCursor.gotoEnd(False)
        oText.insertString(oCursor, "Hello, Writer!", False)
    elif oDoc.supportsService("com.sun.star.sheet.SpreadsheetDocument"):
        oSheet = oDoc.getSheets().getByIndex(0)
        oCell = oSheet.getCellRangeByName("A1")
        oCell.setString("Hello, Calc!")

def get_doc_from_context():
    # We cannot use XSCRIPTCONTEXT in this module because it is not called
    # directly as a macro.
    # The following is possible instead, although it is not needed in this
    # example.
    ctx = uno.getComponentContext()
    smgr = ctx.ServiceManager
    desktop = smgr.createInstanceWithContext(
        "com.sun.star.frame.Desktop", ctx)
    oDoc = desktop.getCurrentComponent()
    return oDoc
"""

def set_up_pythonpath(organizer, tree_node, node):
    """Sets up an example with the following structure.
    python/
        ppath_example.py
        pythonpath/
            loexample/
                __init__.py
                hello_module.py
    """
    node_adder = NodeAdder(organizer, tree_node, node)
    node_adder.add('file', "ppath_example.py", content=PPATH_EXAMPLE_CONTENT)
    node_adder.add('dir', "pythonpath", change_to_dir=True)
    node_adder.add('dir', "loexample", change_to_dir=True)
    node_adder.add('file', "__init__.py")
    node_adder.add('file', "hello_module.py", content=HELLO_CONTENT)
    node_adder.display_results()


class NodeAdder:
    """Create files and directories."""

    def __init__(self, organizer, parent_tree_node, parent_node):
        self.organizer = organizer
        self.items_created = []
        self.parent_tree_node = parent_tree_node
        self.parent_node = parent_node

    def add(self, nodetype, nodename, content="", change_to_dir=False):
        try:
            try:
                if nodetype == 'file':
                    self.organizer.exec_create_file(
                        self.parent_tree_node, self.parent_node, nodename,
                        content)
                else:
                    self.organizer.exec_create_dir(
                        self.parent_tree_node, self.parent_node, nodename)
                self.items_created.append(nodename)
            except AlreadyExistsException as exc:
                pass
            except ErrorAsMessage as exc:
                import traceback
                emessage = traceback.format_exception(
                    None, exc, exc.__traceback__)
                msgbox(str(emessage))
        except Exception as exc:
            import traceback
            emessage = traceback.format_exception(None, exc, exc.__traceback__)
            msgbox(str(emessage))
        if change_to_dir: 
            self.parent_tree_node = self.organizer.tree_get_dir_node(
                self.parent_tree_node, nodename)
            self.parent_node = self.organizer.node_get(self.parent_tree_node)

    def display_results(self):
        if len(self.items_created):
            msg = RR().resolvestring('msg40').format(
                "\n  ".join(self.items_created))
            msgbox(msg)
        else:
            msgbox(RR().resolvestring('msg41'))
